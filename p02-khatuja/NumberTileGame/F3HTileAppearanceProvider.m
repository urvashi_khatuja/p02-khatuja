//
//  F3HTileAppearanceProvider.m
//  NumberTileGame
//
//  Created by Austin Zheng on 3/22/14.
//
//

#import "F3HTileAppearanceProvider.h"

@implementation F3HTileAppearanceProvider

- (UIColor *)tileColorForValue:(NSUInteger)value {
    switch (value) {
        case 2:
            return [UIColor colorWithRed:82./255. green:237./255. blue:199./255. alpha:1];
        case 4:
            return [UIColor colorWithRed:52./255. green:170./255. blue:220./255. alpha:1];
        case 8:
            return [UIColor colorWithRed:26./255. green:214./255. blue:253./255. alpha:1];
        case 16:
            return [UIColor colorWithRed:175./255. green:238./255. blue:238./255. alpha:1];
        case 32:
            return [UIColor colorWithRed:0./255. green:206./255. blue:95./209. alpha:1];
        case 64:
            return [UIColor colorWithRed:32./255. green:178./255. blue:170./255. alpha:1];
        case 128:
        case 256:
        case 512:
        case 1024:
        case 2048:
            
            return [UIColor colorWithRed:237./255. green:207./255. blue:114./255. alpha:1];
        default:
            return [UIColor whiteColor];
    }
}

- (UIColor *)numberColorForValue:(NSUInteger)value {
    switch (value) {
        case 2:
        case 4:
            return [UIColor colorWithRed:119./255. green:110./255. blue:101./255. alpha:1];
        default:
            return [UIColor whiteColor];
    }
}

- (UIFont *)fontForNumbers {
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:20];
}

@end
